class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.integer :order_id
      t.integer :product_id
      t.integer :quantity, default: 0
      t.float :total_payed

      t.timestamps
    end
    add_index :items, :order_id
    add_index :items, :product_id
  end
end
