class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.datetime :purchased_at
			t.float :total_payed
      t.string  :state
      t.string  :address
      t.string  :phone
      
      t.timestamps
    end
    add_index :orders, :user_id
  end
end
