class Order < ApplicationRecord
	belongs_to :user
	has_many :items
	has_many :products, through: :items

	scope :open, ->{where(purchased_at: nil).first_or_create}
	scope :past, ->{where.not(purchased_at: nil)}

	default_scope ->{order(created_at: :desc)}

	validates :user, presence: true

	def store_cart products
		products.each do |k,v|
			item = self.items.where(product_id: k).first_or_create
			item.update(quantity: v)
		end
	end

	def add_one product_id
		item = self.items.where(product_id: product_id).first_or_create
		item.update(quantity: item.quantity + 1)
	end


	def remove_item product_id, complete=nil
		item = self.items.find_by(product_id: product_id)
		if complete
			item.destroy
		else
			item.quantity > 0 ? item.update(quantity: item.quantity - 1) : item.destroy
		end
	end


	def items_hsh
		self.items.inject({}){|hash, item| hash.merge({item.product_id => item.quantity})}
	end

	def total
		self.total_payed or self.products.sum(&:price)
	end

	def pay_with shipping, method=:credits
		self.items.map(&:pay)
		self.update(total_payed: self.total, purchased_at: Time.now, state: shipping[:state], address: shipping[:address], phone: shipping[:phone])
		self.user.update(credit: self.user.credit - self.total) if method == :credits
	end


end
