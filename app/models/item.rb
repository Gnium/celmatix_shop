class Item < ApplicationRecord
	#Relations
	belongs_to :order
	belongs_to :product
	validates :product, presence: true
	validates :order, presence: true

	def pay
		self.update(total_payed: self.product.price)
	end
end
