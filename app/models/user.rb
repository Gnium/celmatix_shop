class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable

  has_many :orders


  validates :fullname, presence: true
    

  def redeem_credit
  	self.update(credit: 10000, bonus_given: true)
  end
end
