class Product < ApplicationRecord
	has_many :items
	has_many :orders, through: :items

	validates :name, presence: true
	validates :brand, presence: true
	validates :model, presence: true
	validates :sku, presence: true
	validates :price, presence: true


	def self.opts field
		["All"] + (self.all.pluck(field).uniq ).sort{|a,b| a <=> b}
	end


	def self.search search_params
		results = self
		results = results.where("LOWER(name) LIKE :term OR LOWER(brand) LIKE :term OR LOWER(model) LIKE :term OR LOWER(sku) LIKE :term OR LOWER(price) LIKE :term OR LOWER(desc) LIKE :term", term: "%#{search_params[:term]}%") if search_params[:term]
		results = results.where(brand: search_params[:brand]) if search_params[:brand] and search_params[:brand] != "All"
		results = results.where("price >= :price", price: search_params[:price_from].to_f) if search_params[:price_from].to_f != 0
		results = results.where("price <= :price", price: search_params[:price_to].to_f) if search_params[:price_to].to_f != 0
		results.all
	end


end
