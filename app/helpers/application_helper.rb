module ApplicationHelper

	def home?
		controller.controller_name == "welcome" and controller.action_name == "home"
	end

	def products?
		controller.controller_name == "products"
	end

	def past_orders?
		controller.controller_name == "cart" and controller.action_name == "past_orders"
	end
end
