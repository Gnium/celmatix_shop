module ProductsHelper


	def product_name id
		Product.find(id).name
	end


	def subtotal id, quantity
		Product.find(id).price * quantity
	end
end
