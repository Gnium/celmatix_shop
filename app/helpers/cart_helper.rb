module CartHelper

	def sum_total
		total = 0
		my_cart.each do |item,quantity|
			total += subtotal(item, quantity)
		end
		total
	end

end
