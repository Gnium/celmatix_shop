class WelcomeController < ApplicationController
  before_action :authenticate_user!, except: [:home]
  def home
    @products = Product.search(search_params)
  end

  def redeem_credit
  	current_user.redeem_credit unless current_user.bonus_given
  	redirect_to :back
  end

  protected
  	def search_params
  		params.permit(:term, :brand, :price_from, :price_to)
  	end
end
