class CartController < ApplicationController
	before_action :authenticate_user!, only: [:checkout]

  def add_item
  	if user_signed_in?
  		@order = current_user.orders.open
  		@order.add_one(params[:id])
  	else
  		session[:items] ||= {}
  		session[:items][params[:id]] ||= 0
  		session[:items][params[:id]] += 1
  	end
  end

  def remove_item
    if user_signed_in?
      @order = current_user.orders.open
      @order.remove_item(params[:id], params[:complete])
    else
      if session[:items][params[:id]] == 0 or params[:complete]
        session[:items].except!(params[:id])
      else
        session[:items][params[:id]] -= 1
      end
    end
  end

  def checkout
    redirect_to products_path unless my_cart.any?
    @order = current_user.orders.open
  end

  def pay_order
    case params[:payment]
    when 1, "1"
      current_user.orders.open.pay_with(shipping_params, :credits)
    end
    redirect_to past_orders_path
  end

  def past_orders
    @orders = current_user.orders.past.to_a
    @recent_order = @orders.shift
  end
  protected

  def shipping_params
  	params.permit(:state, :address, :phone)
  end
end
