class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :my_cart

  protected
  def my_cart
  	if session[:items]
  		session[:items]
  	else
  		user_signed_in? ? current_user.orders.open.items_hsh : {}
  	end
  end


  def after_sign_in_path_for(resource_or_scope)
		current_user.orders.open.store_cart session[:items] if session[:items]
		session[:items] = nil
		super
  end
end
