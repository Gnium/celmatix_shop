class ProductsController < ApplicationController
  def index
  	@products = Product.search(search_params)
  end

  def show
  	@product = Product.find(params[:id])
  end


  protected
  	def search_params
  		params.permit(:term, :brand, :price_from, :price_to)
  	end

end
