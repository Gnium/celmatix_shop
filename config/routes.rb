Rails.application.routes.draw do
  post "add_item", to: 'cart#add_item'
  delete "remove_item", to: 'cart#remove_item'
  get "checkout", to: 'cart#checkout'
  patch "pay_order", to: 'cart#pay_order'
  get 'past_orders', to: 'cart#past_orders'

  patch 'redeem_credit', to: 'welcome#redeem_credit'
  root 'welcome#home'
  resources :products, only: [:index, :show]
  devise_for :users, controllers: {registrations: "registrations"} 
end
