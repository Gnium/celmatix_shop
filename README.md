# README

## This is an eCommerce example implementation.

Main goals required by the client were described like this:

>Your goal is to build a simple e-commerce type web application that fulfills the following requirements:

>* Users can sign up
>* Users can log in
>* Users can view all products in a gridded view
>* Each grid item displays the name of the product, the brand name, the model number and the price
>* See https://www.instagram.com/natgeo/ for an example of a gridded view (but without pictures)
>* Users can click on a product to see details about it in a modal
>* The modal has the same info as the grid view for each product, plus the provided description of the product
>* Users can add products to a cart
>* Add to cart buttons should be on the modal for the product
>* Users can view their cart and the items inside
>* Users can delete an item from their cart
>* Users can log out

>A JSON file with the products can be found attached to the email. Please use this to create the products for the application. The goal is to get as much of the above functionality built as possible. We’re interested more in your approach to how you structure this than how it looks on the front end, so don’t spend all your time designing the style of the front end. The only technical requirement is that you use Rails to build this, otherwise feel free to use whatever gems or libraries you need to accomplish the goals above.

**Features:**

* Database for development is on sql3
* Bootstrap for UI 
* Authentication made using Devise
* UNIT TESTS: RSpec, FactoryGirl, Faker and Devise Test Helpers
* Database has been seeded from json provided

**Additional Features**
* Session based Cart for guest users
* Search form for products
* Initial credit for new users so they can use it at checkout

